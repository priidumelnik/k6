import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 * <p>
 * The main objective of the GraphTask is to check if a random
 * oriented graph contains cycle or not.
 */
public class GraphTask {

    /**
     * Main method.
     * Used for starting the program.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     */
    private void run() {
        Graph g = new Graph("G");

        System.out.println("\n");
        g.createRandomOrientedGraph(2, 1);
        g.checkIfOrientedGraphContainsCycle();

        g.createRandomOrientedGraph(15, 26);
        g.checkIfOrientedGraphContainsCycle();

        g.createRandomOrientedGraph(3, 2);
        g.checkIfOrientedGraphContainsCycle();

        g.createRandomOrientedGraph(3, 3);
        g.checkIfOrientedGraphContainsCycle();

        g.createRandomOrientedGraph(2000, 2500);
        g.startTimer();
        g.checkIfOrientedGraphContainsCycle();
        g.stopTimer();

        g.createRandomOrientedGraph(2000, 1999);
        g.startTimer();
        g.checkIfOrientedGraphContainsCycle();
        g.stopTimer();
    }

    /**
     * Vertex represents one of the vertices in the graph.
     */
    static class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    static class Arc {

        private String id;
        private Vertex target;
        private Arc next;
        private static int info = 0;

        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
    }

    /**
     * Representation of the graph.
     * Without calling the createRandomOrientedGraph(int n, int m) method
     * has no existing structure and cannot be controlled if it contains
     * a cycle.
     */
    static class Graph {

        private String id;
        private Vertex first;
        private static int info = 0;
        private static int info2 = 0;
        private static long time;
        private static Stack<Vertex> cycleFound;
        private static boolean cycleCheckRan = false;
        int CANCEL_GRAPH_AND_CYCLE_PRINT = 20; // Change to alter the maximum number of vertices a graph can have, for it's representation to be displayed.

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append("------------------------------");
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        private Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        private void createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
        }

        /**
         * Create an oriented graph
         * with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        private void createRandomOrientedGraph(int n, int m) {
            checkConditionsForGraph(n, m);
            cycleFound = null;
            cycleCheckRan = false;
            first = null;
            createRandomTree(n);
            int[][] connected = createAdjMatrix();
            info = n;
            info2 = m;
            int edges = m - n + 1;
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            while (edges > 0) {
                int i = (int) (Math.random() * n);
                int j = (int) (Math.random() * n);
                if (i == j)
                    continue;
                if (connected[i][j] == 1 || connected[j][i] == 1)
                    continue;
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                connected[i][j] = 1;
                connected[j][i] = 1;
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                edges--;
            }
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        private void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + (n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        private int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Check if it is possible to create with
         * n number of vertices and m number of edges
         *
         * @param n number of vertices
         * @param m number of edges
         */
        private static void checkConditionsForGraph(int n, int m) {
            if (n <= 0)
                throw new IllegalArgumentException("Number of vertices cannot be 0 or less.");
            if (n > 250000000)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
        }

        /**
         * Check if vertices of the graph contain a cycle.
         * Calls out recOfCycleCheck() method to do actual
         * determination if graph contains a cycle.
         */
        private void checkIfOrientedGraphContainsCycle() {
            cycleCheckRan = true;
            if (first == null)
                throw new RuntimeException("No graph has been initialized.");
            Vertex v = first;
            Stack<Vertex> discovered = new Stack<>();
            List<Arc> arcs = new ArrayList<>();
            while (v != null) {
                discovered.push(v);
                Arc a = v.first;
                while (a != null) {
                    if (!discovered.contains(a.target)) {
                        if (recOfCycleCheck(a.target, discovered, arcs)) {
                            if (info > CANCEL_GRAPH_AND_CYCLE_PRINT) {
                                System.out.println("Graph: G(" + info + "," + info2 + ") " +
                                        "contains a cycle.");
                                return;
                            }
                            System.out.println("Graph: G(" + info + "," + info2 + ") " +
                                    this.toString() +
                                    "contains a cycle.");
                            cycleString();
                            System.out.println("\n");
                            return;
                        } else
                            discovered.pop();
                    }
                    a = a.next;
                }
                discovered.clear();
                v = v.next;
            }
            if (info > CANCEL_GRAPH_AND_CYCLE_PRINT) {
                System.out.println("Graph: G(" + info + "," + info2 + ") \n" +
                        "does not contain a cycle.");
                return;
            }
            System.out.println("Graph: G(" + info + "," + info2 + ") " +
                    this.toString() +
                    "does not contain a cycle.\n");
        }

        /**
         * Check if given input vertex's(input) next vertex is
         * contained inside stack(memory) of already traversed vertices.
         * Method is only called out by checkIfOrientedGraphContainsCycle()
         * and recursively by itself.
         *
         * @param input  vertex to control
         * @param memory stack of traversed vertices
         * @return whether stack contained input vertex's neighboring vertex
         */
        private static boolean recOfCycleCheck(Vertex input, Stack<Vertex> memory, List<Arc> arcs) {
            memory.push(input);
            Arc a = input.first;
            while (a != null) {
                if (!memory.contains(a.target))
                    if (recOfCycleCheck(a.target, memory, arcs))
                        return true;
                    else
                        memory.pop();
                if (memory.contains(a.target)) {
                    memory.push(a.target);
                    cycleFound = memory;
                    return true;
                }
                a = a.next;
            }
            return false;
        }

        /**
         * Remember the time of starting.
         */
        private void startTimer() {
            time = System.nanoTime();
        }

        /**
         * Checks the time of stopping
         * and calculate time between the start
         * of measuring and the end.
         */
        private void stopTimer() {
            long end = System.nanoTime();
            time = end - time;
            System.out.println((time) / 1000000 + " ms to determine whether " +
                    "G(" + info + "," + info2 + ") contains a cycle.\n");
        }

        /**
         * Output arcs of the first detected cycle inside the graph.
         */
        public void cycleString() {
            if (cycleFound == null) {
                if (!cycleCheckRan)
                    throw new RuntimeException("No check for cycle has been conducted for G("
                            + info + "," + info2 + ").");
                else
                    throw new RuntimeException("Graph " + "G("
                            + info + "," + info2 + ") can not contain a cycle.");
            }
            List<Arc> result = new ArrayList<>();
            Arc a = cycleFound.get(0).first;
            int startLoopFrom = 0;
            for (int i = 0; i < cycleFound.size() - 1; i++) {
                if (cycleFound.get(i).id.equals(cycleFound.get(cycleFound.size() - 1).id)) {
                    startLoopFrom = i;
                    a = cycleFound.get(i).first;
                    break;
                }
            }
            for (int i = startLoopFrom; i < cycleFound.size() - 1; i++) {
                while (!a.target.id.equals(cycleFound.get(i + 1).id)) {
                    a = a.next;
                }
                result.add(a);
                a = a.target.first;
            }
            System.out.println("Cycle consists of following arcs: " + result);
        }
    }
}